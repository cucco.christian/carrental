require_relative 'calculator'

# Rent - Class that contains a rent of a car
class Rent
  # rubocop:disable Metrics/ParameterLists
  def initialize(id, cuit, start, type, hours, days, kms)
    @id = id
    @cuit = cuit
    @calculator = Calculator.new.create(cuit, start, type, hours, days, kms)
  end
  # rubocop:enable Metrics/ParameterLists

  def hours
    @calculator.hours
  end

  def days
    @calculator.days
  end

  def kilometers
    @calculator.kilometers
  end

  def cost(end_time)
    @calculator.cost(end_time)
  end

  attr_reader :id, :cuit, :calculator
end
