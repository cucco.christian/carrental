require_relative 'discount'
require_relative 'penality'

# Calculator - Class from which every calculator inherit
class Calculator
  HOUR = 'Horas'.freeze
  KILOMETER = 'Kilometros'.freeze
  DAILY = 'Dia'.freeze

  attr_reader :start_daytime, :discount, :type, :hours, :days, :kilometers

  # rubocop:disable Metrics/ParameterLists
  def create(cuit, start, type, hours, days, kilometer)
    return CalculatorHours.new(cuit, start, hours) if type == HOUR
    return CalculatorKilometers.new(cuit, start, kilometer) if type == KILOMETER
    return CalculatorDaily.new(cuit, start, days) if type == DAILY

    CalculatorPayAsYouGo.new(cuit, start)
  end

  # rubocop:enable Metrics/ParameterLists
  def cost(_end_time)
    0
  end
end

# TO DO: Divide Calculators in different files

# CalculatorKilometers - Class that calculates the price of Kilometer contract
class CalculatorKilometers < Calculator
  KILOMETER_PRICE = 10

  def initialize(cuit, start_daytime, kilometers)
    @start_daytime = start_daytime
    @kilometers = kilometers
    @discount = Discount.new(cuit)
  end

  def cost(_end_time)
    initial_cost = KILOMETER_PRICE * @kilometers
    @discount.apply_discount(initial_cost)
  end
end

# CalculatorHours - Class that calculates the price of Hours contract
class CalculatorHours < Calculator
  HOUR_PRICE = 100
  HOUR = 60 * 60

  def initialize(cuit, start_daytime, hours)
    @start_daytime = start_daytime
    @hours = hours
    @discount = Discount.new(cuit)
    @penality = Penality.new(start_daytime + (HOUR * hours))
  end

  def cost(end_time)
    initial_cost = HOUR_PRICE * @hours
    discounted_cost = @discount.apply_discount(initial_cost)
    @penality.apply_penality(end_time, discounted_cost)
  end
end

# CalculatorDaily - Class that calculates the price of Daily contract
class CalculatorDaily < Calculator
  DAILY_PRICE = 2000
  DAY = 60 * 60 * 24
  def initialize(cuit, start_daytime, days)
    @start_daytime = start_daytime
    @days = days
    @discount = Discount.new(cuit)
    @penality = Penality.new(start_daytime + (DAY * days))
  end

  def cost(end_time)
    initial_cost = DAILY_PRICE * @days
    discounted_cost = @discount.apply_discount(initial_cost)
    @penality.apply_penality(end_time, discounted_cost)
  end
end

# CalculatorPayAsYouGo - Class that calculates the price of PayAsYouGo contract
class CalculatorPayAsYouGo < Calculator
  HOUR = 60 * 60
  HOUR_PRICE = 100
  def initialize(cuit, start_daytime)
    @start_daytime = start_daytime
    @discount = Discount.new(cuit)
  end

  def cost(end_time)
    initial_cost = ((end_time - @start_daytime) / HOUR).round * HOUR_PRICE
    @discount.apply_discount(initial_cost)
  end
end
