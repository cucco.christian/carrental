# Penality - Class that manages penalities
class Penality
  INTERESTS = 1.30
  def initialize(end_time)
    @end_time = end_time
  end

  def apply_penality(returned_time, initial_cost)
    return initial_cost * INTERESTS if returned_time > @end_time

    initial_cost
  end

  attr_reader :end_time
end
