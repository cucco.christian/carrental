# Discount - Class that manages discounts
class Discount
  DISCOUNT = 0.05
  COMPANY_CUIT = '26'.freeze
  def initialize(cuit)
    @cuit = cuit
  end

  def apply_discount(cost)
    starting_cuit = @cuit[0..1]
    return cost - (cost * DISCOUNT) if starting_cuit == COMPANY_CUIT

    cost
  end

  attr_reader :cuit
end
