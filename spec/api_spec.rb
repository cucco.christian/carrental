require 'rspec'
require_relative '../app'
# rubocop:disable Metrics/BlockLength
# rubocop:disable LineLength
describe 'Api' do
  let(:api) { CarRental.new }
  HOUR = 60 * 60
  DAY = HOUR * 24
  it 'Reservar un auto por una hora y espero que el costo sea 100' do
    rent_id = api.rent('2929', Time.new, 'Horas', 1, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 100
  end

  it 'Reservar un auto por dos horas y espero que el costo sea 200' do
    rent_id = api.rent('2929', Time.new, 'Horas', 2, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 200
  end

  it 'Reservar un auto por 100 kilometros y espero que el costo sea 1000' do
    rent_id = api.rent('2929', Time.new, 'Kilometros', 0, 0, 100)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 1000
  end

  it 'Reservar un auto por 50 kilometros y espero que el costo sea 500' do
    rent_id = api.rent('2929', Time.new, 'Kilometros', 0, 0, 50)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 500
  end

  it 'Reservar un auto por un día y espero que el costo sea 2000' do
    rent_id = api.rent('2929', Time.new, 'Dia', 0, 1, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 2000
  end

  it 'Reservar un auto por dos días y espero que el costo sea 4000' do
    rent_id = api.rent('2929', Time.new, 'Dia', 0, 2, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 4000
  end

  it 'Reservar un auto PayAsYouGo, se devuelve una hora despues y espero que el costo sea 100' do
    rent_id = api.rent('2929', Time.new, 'PayAsYouGo', 0, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new + HOUR)
    expect(amount_to_pay).to eq 100
  end

  it 'Reservar un auto PayAsYouGo, se devuelve 4 horas despues y espero que el costo sea 400' do
    rent_id = api.rent('2929', Time.new, 'PayAsYouGo', 0, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new + 4 * HOUR)
    expect(amount_to_pay).to eq 400
  end

  it 'Reservar un auto por una hora con cuit de empresa y espero que el costo sea 95' do
    rent_id = api.rent('2629', Time.new, 'Horas', 1, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 95
  end

  it 'Reservar un auto por 200 kilometros con cuit de empresa y espero que el costo sea 1900' do
    rent_id = api.rent('2629', Time.new, 'Kilometros', 1, 0, 200)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 1900
  end

  it 'Reservar un auto por 5 dias con cuit de empresa y espero que el costo sea 9500' do
    rent_id = api.rent('2629', Time.new, 'Dia', 1, 5, nil)
    amount_to_pay = api.bill(rent_id, Time.new)
    expect(amount_to_pay).to eq 9500
  end

  it 'Reservar un auto PayAsYouGo, se devuelve 5 horas despues y espero que el costo sea 480 por tener cuit de empresa' do
    rent_id = api.rent('2629', Time.new, 'PayAsYouGo', 0, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new + 5 * HOUR)
    expect(amount_to_pay).to eq 475
  end

  it 'Reservar un auto por una hora, se entrega mas tarde y espero que el costo sea 130' do
    rent_id = api.rent('2929', Time.new, 'Horas', 1, 0, nil)
    amount_to_pay = api.bill(rent_id, Time.new + (2 * HOUR))
    expect(amount_to_pay).to eq 130
  end

  it 'Reservar un auto por dos dias, se entrega mas tarde y espero que el costo sea 5200' do
    rent_id = api.rent('2929', Time.new, 'Dia', 1, 2, nil)
    amount_to_pay = api.bill(rent_id, Time.new + (2.1 * DAY))
    expect(amount_to_pay).to eq 5200
  end

  it 'Reservar un auto por dos dias con cuit de empresa, se entrega mas tarde y espero que el costo sea 5200' do
    rent_id = api.rent('2629', Time.new, 'Dia', 1, 2, nil)
    amount_to_pay = api.bill(rent_id, Time.new + (2.1 * DAY))
    expect(amount_to_pay).to eq 4940
  end

  it 'Reservar un auto por dos horas con cuit de empresa, se entrega mas tarde y espero que el costo sea 247' do
    rent_id = api.rent('2629', Time.new, 'Horas', 2, 1, nil)
    amount_to_pay = api.bill(rent_id, Time.new + (2.1 * DAY))
    expect(amount_to_pay).to eq 247
  end
end
# rubocop:enable Metrics/BlockLength
# rubocop:enable LineLength
