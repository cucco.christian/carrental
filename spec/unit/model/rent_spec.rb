require 'rspec'
require_relative '../../../model/rent'
# rubocop:disable Metrics/BlockLength
# rubocop:disable LineLength
describe 'Rent' do
  HOUR = 60 * 60
  DAY = HOUR * 24
  it 'Crear reserva  y verifico ID y cuit' do
    reservation = Rent.new('1', '2929', Time.new, 'Horas', 1, 1, nil)
    expect(reservation.id).to eq '1'
    expect(reservation.cuit).to eq '2929'
  end

  it 'Crear reserva y verifico ID y cuit con otros datos' do
    reservation = Rent.new('2', '3030', Time.new, 'Horas', 1, 1, nil)
    expect(reservation.id).to eq '2'
    expect(reservation.cuit).to eq '3030'
  end

  it 'Abonar reserva por hora con una hora y que cueste 100' do
    reservation = Rent.new('1', '2929', Time.new, 'Horas', 1, 1, nil)
    expect(reservation.cost(Time.new)).to eq 100
  end

  it 'Abonar reserva por hora con 2 horas y que cueste 200' do
    reservation = Rent.new('1', '2929', Time.new, 'Horas', 2, 1, nil)
    expect(reservation.cost(Time.new)).to eq 200
  end

  it 'Abonar reserva por 100 kilometros  y que cueste 1000' do
    reservation = Rent.new('1', '2929', Time.new, 'Kilometros', 1, 1, 100)
    expect(reservation.cost(Time.new)).to eq 1000
  end

  it 'Abonar reserva por 200 kilometros  y que cueste 2000' do
    reservation = Rent.new('1', '2929', Time.new, 'Kilometros', 1, 1, 200)
    expect(reservation.cost(Time.new)).to eq 2000
  end

  it 'Abonar reserva por un día y que cueste 2000' do
    reservation = Rent.new('1', '2929', Time.new, 'Dia', 1, 1, nil)
    expect(reservation.cost(Time.new)).to eq 2000
  end

  it 'Abonar reserva por dos dias y que cueste 4000' do
    reservation = Rent.new('1', '2929', Time.new, 'Dia', 1, 2, nil)
    expect(reservation.cost(Time.new)).to eq 4000
  end

  it 'Abonar reserva PayAsYouGo por una hora y que cueste 100' do
    reservation = Rent.new('1', '2929', Time.new, 'PayAsYouGo', 1, 2, nil)
    expect(reservation.cost(Time.new + HOUR)).to eq 100
  end

  it 'Abonar reserva PayAsYouGo por cuatro horas y que cueste 400' do
    reservation = Rent.new('1', '2929', Time.new, 'PayAsYouGo', 1, 2, nil)
    expect(reservation.cost(Time.new + (4 * HOUR))).to eq 400
  end

  it 'Abonar reserva por hora con una hora y que cueste 95 por tener cuit de empresa' do
    reservation = Rent.new('1', '2629', Time.new, 'Horas', 1, 1, nil)
    expect(reservation.cost(Time.new)).to eq 95
  end

  it 'Abonar reserva por 10 kilometros y que cueste 95 por tener cuit de empresa' do
    reservation = Rent.new('1', '2629', Time.new, 'Kilometros', 1, 1, 10)
    expect(reservation.cost(Time.new)).to eq 95
  end

  it 'Abonar reserva por 1 dia y que cueste 1900 por tener cuit de empresa' do
    reservation = Rent.new('1', '2629', Time.new, 'Dia', 1, 1, 10)
    expect(reservation.cost(Time.new)).to eq 1900
  end

  it 'Abonar reserva PayAsYouGo por una horas y que cueste 95 por tener cuit de empresa' do
    reservation = Rent.new('1', '2629', Time.new, 'PayAsYouGo', 1, 2, nil)
    expect(reservation.cost(Time.new + (1 * HOUR))).to eq 95
  end

  it 'Abonar reserva por hora con tres horas, se entrega mas tarde y espero que cueste 390' do
    reservation = Rent.new('1', '2929', Time.new, 'Horas', 3, 1, nil)
    expect(reservation.cost(Time.new + (5 * HOUR))).to eq 390
  end

  it 'Abonar reserva por dia con 1 dia, se entrega mas tarde y espero que cueste 2600' do
    reservation = Rent.new('1', '2929', Time.new, 'Dia', 0, 1, nil)
    expect(reservation.cost(Time.new + (1.8 * DAY))).to eq 2600
  end

  it 'Abonar reserva por dia con 1 dia con cuit de empresa, se entrega mas tarde y espero que cueste 2600' do
    reservation = Rent.new('1', '2629', Time.new, 'Dia', 0, 1, nil)
    expect(reservation.cost(Time.new + (1.8 * DAY))).to eq 2470
  end

  it 'Abonar reserva por hora con tres horas con cuit de empresa, se entrega mas tarde y espero que cueste 370,50' do
    reservation = Rent.new('1', '2629', Time.new, 'Horas', 3, 1, nil)
    expect(reservation.cost(Time.new + (5 * HOUR))).to eq 370.50
  end
end
# rubocop:enable Metrics/BlockLength
# rubocop:enable LineLength
