require 'rspec'
require_relative '../../../model/calculator'
# rubocop:disable LineLength
# rubocop:disable BlockLength
describe 'Calculator Hours' do
  HOUR = 60 * 60
  it 'Crear Calculator y espero que las horas sean 1' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 1, 1, nil)
    expect(calculator.hours).to eq 1
  end

  it 'Crear Calculator  y espero que las horas sean 2' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 2, 1, nil)
    expect(calculator.hours).to eq 2
  end

  it 'Calcular precio por una hora y espero que el precio sea 100' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 1, 1, nil)
    expect(calculator.cost(Time.new)).to eq 100
  end

  it 'Calcular precio por dos horas y espero que el precio sea 200' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 2, 1, nil)
    expect(calculator.cost(Time.new)).to eq 200
  end

  it 'Calcular precio por dos horas con cuit de empresa y espero que el precio sea 190' do
    calculator = Calculator.new.create('2620', Time.new, 'Horas', 2, 1, nil)
    expect(calculator.cost(Time.new)).to eq 190
  end

  it 'Calcular precio por dos horas, se entrega tarde y espero que el precio sea 260' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 2, 1, nil)
    expect(calculator.cost(Time.new + (HOUR * 2.1))).to eq 260
  end

  it 'Calcular precio por dos horas con cuit de empresa, se entrega tarde y espero que el precio sea 247' do
    calculator = Calculator.new.create('2620', Time.new, 'Horas', 2, 1, nil)
    expect(calculator.cost(Time.new + (HOUR * 2.1))).to eq 247
  end
end
# rubocop:enable LineLength
# rubocop:enable BlockLength
