require 'rspec'
require_relative '../../../model/penality'

describe 'Penality' do
  it 'Crear Penality' do
    time = Time.new
    penality = Penality.new(time)
    expect(penality.end_time).to eq time
  end

  it 'Creo Penality de un costo de 100 y espero que el resultado sea 130' do
    time = Time.new
    penality = Penality.new(time)
    expect(penality.apply_penality(time + 1, 100)).to eq 130
  end

  it 'Creo Penality de un costo de 200 y espero que el resultado sea 260' do
    time = Time.new
    penality = Penality.new(time)
    expect(penality.apply_penality(time + 1, 200)).to eq 260
  end
end
