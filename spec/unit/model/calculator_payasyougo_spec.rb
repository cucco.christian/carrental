require 'rspec'
require_relative '../../../model/calculator'
# rubocop:disable LineLength
describe 'Calculator PayAsYouGo y verifico el tiempo de inicio' do
  HOUR = 60 * 60
  it 'Crear Calculator' do
    time = Time.new
    calculator = Calculator.new.create('2020', time, 'PayAsYouGo', 0, 0, 0)
    expect(calculator.start_daytime).to eq time
  end

  it 'Calcular precio por una hora con contrato PayAsYouGo y que cueste 100' do
    calculator = Calculator.new.create('2020', Time.new, 'PayAsYouGo', 0, 0, 0)
    expect(calculator.cost(Time.new + HOUR)).to eq 100
  end

  it 'Calcular precio por 7 horas con contrato PayAsYouGo y que cueste 700' do
    calculator = Calculator.new.create('2020', Time.new, 'PayAsYouGo', 0, 0, 0)
    expect(calculator.cost(Time.new + (7 * HOUR))).to eq 700
  end

  it 'Calcular precio por 10 horas con contrato PayAsYouGo y que cueste 950 por tener cuit de empresa' do
    calculator = Calculator.new.create('2620', Time.new, 'PayAsYouGo', 0, 0, 0)
    expect(calculator.cost(Time.new + (10 * HOUR))).to eq 950
  end
end
# rubocop:enable LineLength
