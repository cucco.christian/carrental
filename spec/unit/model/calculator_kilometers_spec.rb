require 'rspec'
require_relative '../../../model/calculator'
# rubocop:disable LineLength
describe 'Calculator Kilometers' do
  it 'Crear Calculator y espero que la cantidad de kilometros sea 100' do
    calculator = Calculator.new.create('20', Time.new, 'Kilometros', 0, 0, 100)
    expect(calculator.kilometers).to eq 100
  end

  it 'Crear Calculator y espero que la cantidad de kilometros sea 200' do
    calculator = Calculator.new.create('20', Time.new, 'Kilometros', 0, 0, 200)
    expect(calculator.kilometers).to eq 200
  end

  it 'Calcular precio por 100 km y espero que cueste 1000' do
    calculator = Calculator.new.create('20', Time.new, 'Kilometros', 0, 0, 100)
    expect(calculator.cost(Time.new)).to eq 1000
  end

  it 'Calcular precio por 200 km y espero que cueste 2000' do
    calculator = Calculator.new.create('20', Time.new, 'Kilometros', 0, 0, 200)
    expect(calculator.cost(Time.new)).to eq 2000
  end

  it 'Calcular precio por 200 km y espero que cueste 1900' do
    calculator = Calculator.new.create('2671', Time.new, 'Kilometros', 0, 0, 200)
    expect(calculator.cost(Time.new)).to eq 1900
  end
end
# rubocop:enable LineLength
