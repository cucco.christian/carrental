require 'rspec'
require_relative '../../../model/discount'
# rubocop:disable Metrics/LineLength
describe 'Discount' do
  it 'Crear Discount' do
    discount = Discount.new('2020')
    expect(discount.cuit).to eq '2020'
  end

  it 'Creo un descuento con un monto inicial de 100 y espero que devuelva 95' do
    discount = Discount.new('2620')
    expect(discount.apply_discount(100)).to eq 95
  end

  it 'Creo un descuento con un monto inicial de 200 y espero que devuelva 190' do
    discount = Discount.new('2620')
    expect(discount.apply_discount(200)).to eq 190
  end

  it 'Creo un descuento con un monto inicial de 200 y espero que devuelva 200 porque el cuit no lo debe recibir' do
    discount = Discount.new('2020')
    expect(discount.apply_discount(200)).to eq 200
  end
end
# rubocop:enable Metrics/LineLength
