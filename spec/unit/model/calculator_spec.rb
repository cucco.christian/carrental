require 'rspec'
require_relative '../../../model/calculator'

describe 'Calculator' do
  it 'Crear Calculator' do
    calculator = Calculator.new.create('2020', Time.new, 'Horas', 1, 1, nil)
    expect(calculator.hours).to eq 1
  end

  it 'Calcular precio' do
    calculator = Calculator.new
    expect(calculator.cost(Time.new)).to eq 0
  end
end
