require 'rspec'
require_relative '../../../model/calculator'
# rubocop:disable LineLength
# rubocop:disable BlockLength
describe 'Calculator Daily' do
  DAY = 60 * 60 * 24
  it 'Crear Calculator y espero que los dias sean igual a 1' do
    calculator = Calculator.new.create('2020', Time.new, 'Dia', 1, 1, nil)
    expect(calculator.days).to eq 1
  end

  it 'Crear Calculator y espero que los dias sean igual a 2' do
    calculator = Calculator.new.create('2020', Time.new, 'Dia', 1, 2, nil)
    expect(calculator.days).to eq 2
  end

  it 'Calcular precio contratado por un día y el precio es 2000' do
    calculator = Calculator.new.create('2020', Time.new, 'Dia', 1, 1, nil)
    expect(calculator.cost(Time.new)).to eq 2000
  end

  it 'Calcular precio contratado por dos día y el precio es 4000' do
    calculator = Calculator.new.create('2020', Time.new, 'Dia', 1, 2, nil)
    expect(calculator.cost(Time.new)).to eq 4000
  end

  it 'Calcular precio contratado por un día y el precio es 1900 por tener cuit de empresa' do
    calculator = Calculator.new.create('2620', Time.new, 'Dia', 1, 1, nil)
    expect(calculator.cost(Time.new)).to eq 1900
  end

  it 'Calcular precio contratado por un día, se entrega tarde y se espera que el costo sea 2600' do
    calculator = Calculator.new.create('2920', Time.new, 'Dia', 1, 1, nil)
    expect(calculator.cost(Time.new + (1.1 * DAY))).to eq 2600
  end

  it 'Calcular precio contratado por un día con cuit de empresa, se entrega tarde y se espera que el costo sea 2470' do
    calculator = Calculator.new.create('2620', Time.new, 'Dia', 1, 1, nil)
    expect(calculator.cost(Time.new + (1.1 * DAY))).to eq 2470
  end
end
# rubocop:enable LineLength
# rubocop:enable BlockLength
