require 'json'
require_relative 'model/rent'

# CarRental - Api for reserving cars
class CarRental
  def initialize
    @db = []
  end

  # rubocop:disable Metrics/ParameterLists
  def rent(cuit, start, type, hours, days, kilometers)
    rent = Rent.new(@db.length, cuit, start, type, hours, days, kilometers)
    @db << rent
    rent.id
  end
  # rubocop:enable Metrics/ParameterLists

  def bill(rent_id, end_time)
    @db.each { |rent| return rent.cost(end_time) if rent.id == rent_id }
  end
end
